package practica;

import java.sql.*;
import java.io.FileWriter;
import java.io.IOException;
import com.google.gson.Gson;

public class JSONexport {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String dbUrl = "jdbc:mysql://localhost:3306/myDatabase"; 
		String user="root";
		String password="mysqlpassword";
		
		try{ Class.forName("org.gjt.mm.mysql.Driver"); }
		catch(ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error loading the driver!\n");
		}
		
		try{  
			Connection c=DriverManager.getConnection(dbUrl, user, password);
			Statement s = c.createStatement();
			
			ResultSet r = s.executeQuery(" SELECT number,name,job FROM myTable WHERE MyTable.number='3'");
			
			Table myTable = new Table();
			
			if (r.next())
			{   
				myTable.setId(r.getInt("number"));
				myTable.setName(r.getString("name"));
				myTable.setJob(r.getString("job"));
			}
			
			Gson gson = new Gson();
			String json = gson.toJson(myTable);
			
			try {
				   FileWriter writer = new FileWriter("C:\\Users\\etern\\Desktop\\Practica\\jsonfile.json");
				   writer.write(json);
				   writer.close();
				  
			} catch (IOException e) {
				e.printStackTrace();
				}
				  
			System.out.println("Done creating JSON file");
			
			r.close();
			s.close();
		}catch(SQLException e) {e.printStackTrace();}
		
	}

}
