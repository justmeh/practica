package practica;

import java.sql.*;

import java.io.File;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import org.w3c.dom.*;

public class XMLexport {
	
	public static final String xmlFilePath = "C:\\Users\\etern\\Desktop\\Practica\\xmlfile.xml";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String dbUrl = "jdbc:mysql://localhost:3306/myDatabase";
		String user="root";
		String password="mysqlpassword";
		

		try{ Class.forName("org.gjt.mm.mysql.Driver"); }
		catch(ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error loading the driver!\n");
		}
		
		try{  
			Connection c=DriverManager.getConnection(dbUrl, user, password);
			Statement s = c.createStatement();
			
			ResultSet r = s.executeQuery(" SELECT number,name,job FROM myTable WHERE MyTable.number='2'");
			
			try { 
				DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
				Document document = documentBuilder.newDocument();
				
				// root element
				Element root = document.createElement("table");
				document.appendChild(root);
				
				// employee element
				Element person = document.createElement("person");
				root.appendChild(person);
				
				
				if(r.next())
				{
					person.setAttribute("id", r.getString("number"));
					
					// name element
					Element name = document.createElement("name");
					name.appendChild(document.createTextNode(r.getString("name")));
					person.appendChild(name);
					
					// job element
					Element job = document.createElement("job");
					job.appendChild(document.createTextNode(r.getString("job")));
					person.appendChild(job);
				}
			
				// create the xml file
				//transform the DOM Object to an XML File
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource domSource = new DOMSource(document);
				StreamResult streamResult = new StreamResult(new File(xmlFilePath));
				
			// If you use
				// StreamResult result = new StreamResult(System.out);
				// the output will be pushed to the standard output ...
				// You can use that for debugging
				
				transformer.transform(domSource, streamResult);
				
				System.out.println("Done creating XML File");
				
			} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
			} catch (TransformerException tfe) {
			tfe.printStackTrace();
			}
			
			r.close();
			s.close();
			
		}catch(SQLException e) {e.printStackTrace();}
		
	}

}
