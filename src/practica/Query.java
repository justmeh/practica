package practica;

import java.sql.*;

public class Query {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//credentials and the server URL. change the IP if the server is not on the same machine
		String dbUrl = "jdbc:mysql://localhost:3306/myDatabase"; // use your port (slide 3)
		String user="root";
		String password="mysqlpassword";
		//load the driver

		try{ Class.forName("org.gjt.mm.mysql.Driver"); }
		catch(ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error loading the driver!\n");
		}

		try{  //create a connection using the driver, try to do this only once per application for better efficiency.
			Connection c=DriverManager.getConnection(dbUrl, user, password);
			Statement s = c.createStatement();
			
			//create a query
			ResultSet r = s.executeQuery(" SELECT job FROM myTable WHERE myTable.name='John' AND myTable.number=1  ");
			
			
			if (r.next())
			{   
				System.out.println("John's job is:"+r.getString("job"));
			}
			
			r.close();
			s.close();
		}catch(SQLException e) {e.printStackTrace();}

		// use this code in whatever java application you wish: java app, applet, servlet, web service, etc. And YES , you DO have to change the queries and tables to make it do what YOU want it to do.
	}

}
