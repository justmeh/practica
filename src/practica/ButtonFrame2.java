package practica;

import java.awt.*; 
import java.awt.event.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.*;

class ButtonFrame2 extends JFrame implements ActionListener
{
	String dbUrl = "jdbc:mysql://localhost:3306/myDatabase"; // use your port (slide 3)
	String user="root";
	String password="mysqlpassword";
	
	JButton bChange ; // reference to the button object

  // constructor for ButtonFrame2
  ButtonFrame2(String title) 
  {
    super( title );                   // invoke the JFrame constructor
    setLayout( new FlowLayout() );    // set the layout manager

    // construct a Button
    bChange = new JButton("Click Me!"); 
    
    // register the ButtonFrame2 object as the listener for the JButton.
    bChange.addActionListener( this ); 

    add( bChange );                   // add the button to the JFrame
    setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );   
  }
  
  public void actionPerformed( ActionEvent evt)
  {
	  try{ Class.forName("org.gjt.mm.mysql.Driver"); }
		catch(ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error loading the driver!\n");
		}
		
		try{
			Connection c = DriverManager.getConnection(dbUrl, user, password);
			Statement s = c.createStatement();
			
			List<String> insert = new  ArrayList<String>();
			insert.add("insert into myTable(number,name) values(1, 'Tyler');");
			insert.add("insert into myTable(number,name) values(2, 'Bob');");
			insert.add("insert into myTable(number,name) values(3, 'Mark');");
			insert.add("insert into myTable(number,name) values(4,'Aaron');");
			insert.add("insert into myTable(number,name) values(5,'Abe');");
			insert.add("insert into myTable(number,name) values(6,'Bach');");
			insert.add("insert into myTable(number,name) values(7,'Vincent');");
			insert.add("insert into myTable(number,name) values(8,'Valencia');");
			insert.add("insert into myTable(number,name) values(9,'Sabrina');");
			insert.add("insert into myTable(number,name) values(10,'Sachi');");
			insert.add("insert into myTable(number,name) values(11,'Miley');");
			insert.add("insert into myTable(number,name) values(12,'Katy');");
			insert.add("insert into myTable(number,name) values(13,'Mark');");
			insert.add("insert into myTable(number,name) values(14,'Bob');");
			insert.add("insert into myTable(number,name) values(15,'Sam');");
			insert.add("insert into myTable(number,name) values(16,'Dacey');");
			insert.add("insert into myTable(number,name) values(17,'Pablo');");
			insert.add("insert into myTable(number,name) values(18,'Calvin');");
			insert.add("insert into myTable(number,name) values(19,'Dahlia');");
			insert.add("insert into myTable(number,name) values(20,'Tania');");
			insert.add("insert into myTable(number,name) values(21,'Sander');");
			insert.add("insert into myTable(number,name) values(22,'Rebecca');");
			insert.add("insert into myTable(number,name) values(23,'Darius');");
			insert.add("insert into myTable(number,name) values(24,'Orlando');");
			insert.add("insert into myTable(number,name) values(25,'Darwin');");
			insert.add("insert into myTable(number,name) values(26,'Edison');");
			insert.add("insert into myTable(number,name) values(27,'Caesar');");
			insert.add("insert into myTable(number,name) values(28,'Paul');");
			insert.add("insert into myTable(number,name) values(29,'Zack');");
			insert.add("insert into myTable(number,name) values(30,'Dan');");
			insert.add("insert into myTable(number,name) values(31,'Peter');");
			insert.add("insert into myTable(number,name) values(32,'Edward');");
			insert.add("insert into myTable(number,name) values(33,'Cassandra');");
			insert.add("insert into myTable(number,name) values(34,'Carla');");
			insert.add("insert into myTable(number,name) values(35,'Elena');");
			insert.add("insert into myTable(number,name) values(36,'Damon');");
			insert.add("insert into myTable(number,name) values(37,'Oliver');");
			insert.add("insert into myTable(number,name) values(38,'Raisa');");
			insert.add("insert into myTable(number,name) values(39,'Albert');");
			insert.add("insert into myTable(number,name) values(40,'Tyrone');");
			insert.add("insert into myTable(number,name) values(41,'Claudia');");
			insert.add("insert into myTable(number,name) values(42,'Darla');");
			insert.add("insert into myTable(number,name) values(43,'Octavian');");
			insert.add("insert into myTable(number,name) values(44,'Cara');");
			insert.add("insert into myTable(number,name) values(45,'Bill');");
			insert.add("insert into myTable(number,name) values(46,'Tom');");
			insert.add("insert into myTable(number,name) values(47,'Ronald');");
			insert.add("insert into myTable(number,name) values(48,'Dana');");
			insert.add("insert into myTable(number,name) values(49,'Tad');");
			insert.add("insert into myTable(number,name) values(50,'Rafael');");
			c.setAutoCommit(false);
			
			Iterator itr=insert.iterator();
			while(itr.hasNext())
			{  
				   s.addBatch((String) itr.next());
			} 
		    
		    s.executeBatch();
		    c.commit();
		      
			s.close();
		}catch(SQLException e) {e.printStackTrace();}
	}
}